function(add_framework_python_module FRAMEWORKS_PACKAGE_NAME)
    set(oneValueArgs BINDINGS_HEADER BINDINGS_TYPESYSTEM SOURCE_DIR)
    set(multiValueArgs QT_DEPENDENCIES WRAPPERS)
    cmake_parse_arguments(ADD_FRAMEWORK_PYTHON_MODULE "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    string(REPLACE "KF6" "KF6::" _FRAMEWORK_TARGET ${FRAMEWORKS_PACKAGE_NAME})
    string(REPLACE "KF6" "K" _FRAMEWORK_BINDINGS_LIBRARY ${FRAMEWORKS_PACKAGE_NAME})

    find_package(${FRAMEWORKS_PACKAGE_NAME} REQUIRED)

    if(${FRAMEWORKS_PACKAGE_NAME}_FOUND)
        get_target_property(${FRAMEWORKS_PACKAGE_NAME}_INCLUDE_DIR ${_FRAMEWORK_TARGET} INTERFACE_INCLUDE_DIRECTORIES)
        get_target_property(PySide6_INCLUDE_BASEDIR PySide6::pyside6 INTERFACE_INCLUDE_DIRECTORIES)

        set(_WRAPPER_BASEDIR ${CMAKE_BINARY_DIR}/wrappers/${FRAMEWORKS_PACKAGE_NAME})
        set(_GENERATOR_INCLUDE_DIRS
            ${PySide6_INCLUDE_BASEDIR}
            ${SHIBOKEN_PYTHON_INCLUDE_DIRS}
            ${${FRAMEWORKS_PACKAGE_NAME}_INCLUDE_DIR}
        )
        foreach(_qtmodule ${ADD_FRAMEWORK_PYTHON_MODULE_QT_DEPENDENCIES})
            string(REPLACE "Qt6::" "" _qtmodule_basename ${_qtmodule})
            list(APPEND _GENERATOR_INCLUDE_DIRS ${Qt6${_qtmodule_basename}_INCLUDE_DIRS} ${PySide6_INCLUDE_BASEDIR}/Qt${_qtmodule_basename})
        endforeach()

        foreach(wrapper ${ADD_FRAMEWORK_PYTHON_MODULE_WRAPPERS})
            list(APPEND _GENERATED_SOURCES ${_WRAPPER_BASEDIR}/${_FRAMEWORK_BINDINGS_LIBRARY}/${wrapper})
        endforeach()

        # Add custom target to run shiboken to generate the binding cpp files.
        add_custom_command(
            OUTPUT ${_GENERATED_SOURCES}
            COMMAND shiboken6
                        --debug-level=medium
                        --generator-set=shiboken
                        --enable-parent-ctor-heuristic
                        --enable-pyside-extensions
                        --use-isnull-as-nb_nonzero
                        --language-level=c++20
                        --typesystem-paths=${PYSIDE_TYPESYSTEMS}
                        --output-directory=${_WRAPPER_BASEDIR}
                        "-I$<JOIN:${_GENERATOR_INCLUDE_DIRS},;-I>"
                        ${ADD_FRAMEWORK_PYTHON_MODULE_BINDINGS_HEADER}
                        ${ADD_FRAMEWORK_PYTHON_MODULE_BINDINGS_TYPESYSTEM}
            COMMAND_EXPAND_LISTS
            DEPENDS ${ADD_FRAMEWORK_PYTHON_MODULE_BINDINGS_HEADER} ${ADD_FRAMEWORK_PYTHON_MODULE_BINDINGS_TYPESYSTEM}
            IMPLICIT_DEPENDS CXX ${ADD_FRAMEWORK_PYTHON_MODULE_BINDINGS_HEADER}
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
            COMMENT "Running bindings generator for ${ADD_FRAMEWORK_PYTHON_MODULE_BINDINGS_TYPESYSTEM}."
        )
        add_custom_target(${_FRAMEWORK_BINDINGS_LIBRARY}_generator ALL DEPENDS ${ADD_FRAMEWORK_PYTHON_MODULE_BINDINGS_HEADER} ${ADD_FRAMEWORK_PYTHON_MODULE_BINDINGS_TYPESYSTEM} ${_GENERATED_SOURCES})

        # Define and build the bindings library.
        add_library(${_FRAMEWORK_BINDINGS_LIBRARY} MODULE ${_GENERATED_SOURCES})
        add_dependencies(${_FRAMEWORK_BINDINGS_LIBRARY} ${_FRAMEWORK_BINDINGS_LIBRARY}_generator)
        set_property(TARGET ${_FRAMEWORK_BINDINGS_LIBRARY} PROPERTY PREFIX "")
        target_include_directories(${_FRAMEWORK_BINDINGS_LIBRARY} PRIVATE ${_GENERATOR_INCLUDE_DIRS})
        target_link_libraries(${_FRAMEWORK_BINDINGS_LIBRARY}
            PRIVATE
                ${ADD_FRAMEWORK_PYTHON_MODULE_QT_DEPENDENCIES}
                PySide6::pyside6
                Shiboken6::libshiboken
                ${_FRAMEWORK_TARGET}
        )
        get_property(_GENERATED_FRAMEWORKS_MODULES GLOBAL PROPERTY GENERATED_FRAMEWORKS_MODULES)
        set_property(GLOBAL PROPERTY GENERATED_FRAMEWORKS_MODULES "${_FRAMEWORK_BINDINGS_LIBRARY};${_GENERATED_FRAMEWORKS_MODULES}")

        if(Doxygen_FOUND)
            if(BUILD_DOCS)
                if(FRAMEWORKS_SOURCE_ROOT)
                    if(EXISTS ${FRAMEWORKS_SOURCE_ROOT}/${ADD_FRAMEWORK_PYTHON_MODULE_SOURCE_DIR})
                        message(STATUS "Found ${FRAMEWORKS_PACKAGE_NAME} source at ${FRAMEWORKS_SOURCE_ROOT}/${ADD_FRAMEWORK_PYTHON_MODULE_SOURCE_DIR}. Setting up documentation generation")

                        doxygen_add_docs(${_FRAMEWORK_BINDINGS_LIBRARY}_source_doxygen_docs
                            ${FRAMEWORKS_SOURCE_ROOT}/${ADD_FRAMEWORK_PYTHON_MODULE_SOURCE_DIR} ALL
                            COMMENT "Generate doxygen docs for ${_FRAMEWORK_BINDINGS_LIBRARY}"
                        )

                        add_custom_target(${_FRAMEWORK_BINDINGS_LIBRARY}_py_docs ALL
                            COMMAND shiboken6
                                        --debug-level=medium
                                        --generator-set=qtdoc
                                        --doc-parser=doxygen
                                        --library-source-dir=${FRAMEWORKS_SOURCE_ROOT}/${ADD_FRAMEWORK_PYTHON_MODULE_SOURCE_DIR}
                                        --documentation-data-dir=${CMAKE_CURRENT_BINARY_DIR}/xml
                                        --enable-pyside-extensions
                                        --typesystem-paths=${PYSIDE_TYPESYSTEMS}
                                        --output-directory=${CMAKE_BINARY_DIR}/docs/
                                        "-I$<JOIN:${_GENERATOR_INCLUDE_DIRS},;-I>"
                                        ${ADD_FRAMEWORK_PYTHON_MODULE_BINDINGS_HEADER}
                                        ${ADD_FRAMEWORK_PYTHON_MODULE_BINDINGS_TYPESYSTEM}
                            COMMAND_EXPAND_LISTS
                            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
                            COMMENT "Running docs generator for ${ADD_FRAMEWORK_PYTHON_MODULE_BINDINGS_TYPESYSTEM}."
                        )

                    else()
                        message(WARNING "Could not find ${FRAMEWORKS_PACKAGE_NAME} source at ${FRAMEWORKS_SOURCE_ROOT}/${ADD_FRAMEWORK_PYTHON_MODULE_SOURCE_DIR}. Skipping documentation generation")
                    endif()
                else()
                    message(WARNING "FRAMEWORKS_SOURCE_ROOT is not set. Point FRAMEWORKS_SOURCE_ROOT to a directory containing the frameworks sources.\nIf you are using kdesrc, enter the source dir path here")
                    message(WARNING "Skipping documentation generation")
                endif()
            else()
                message(STATUS "BUILD_DOCS is Off. Not generating documentation")
            endif(BUILD_DOCS)
        else()
            message(WARNING "Doxygen not found. Skipping documentation generation")
        endif()
    endif()
endfunction()
