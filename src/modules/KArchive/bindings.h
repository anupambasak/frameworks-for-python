#pragma once

#include <sys/types.h>
#include "KArchive"
#include "KArchiveEntry"
#include "KArchiveFile"
#include "KArchiveDirectory"
#include "KAr"
#include "KCompressionDevice"
#include "KFilterBase"
#include "KRcc"
#include "KTar"
#include "KZip"
#include "KZipFileEntry"
#include "K7Zip"
