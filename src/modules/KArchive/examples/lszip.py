#!/usr/bin/python3

from KF6.KArchive import KZip
from PySide6.QtCore import QIODevice
import sys


if __name__ == "__main__":
    zip_path = sys.argv[1]
    zip_dir = sys.argv[2] if len(sys.argv) == 3 else "/"

    archive = KZip(zip_path)
    if archive.open(QIODevice.ReadOnly):
        dir = archive.directory().entry(zip_dir)

        if dir is not None and dir.isDirectory():
            print("Type    Name")
            print("----    -------------------")

            for entry_name in dir.entries():
                entry = dir.entry(entry_name)
                print(f"{' dir' if entry.isDirectory() else 'file'}    {entry.name()}")
        else:
            print(f"ERROR : Path `{zip_dir}` is not a directory in {zip_path}")

        archive.close()
    else:
        print(f"ERROR : Cannot open file because {archive.errorString()}")
