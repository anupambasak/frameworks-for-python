#!/usr/bin/python3

from KF6.KCodecs import KCodecs
import sys


if __name__ == "__main__":
    if sys.argv[1] == "encode":
        print(KCodecs.base64Encode(sys.argv[2].encode("utf-8")))

    if sys.argv[1] == "decode":
        print(KCodecs.base64Decode(sys.argv[2].encode("utf-8")))
